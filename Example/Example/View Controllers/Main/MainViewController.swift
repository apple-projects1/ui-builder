//
//  MainViewController.swift
//  Example
//
//  Created by underrateddev on 2020-03-16.
//  Copyright © 2020 underrateddev. All rights reserved.
//

import UIKit

import UIBuilder

class MainViewController: UIViewController {
    
    let firstView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        
        return view
    }()
    
    let secondView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .green
        
        return view
    }()
    
    let thirdView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        
        return view
    }()
    
    let fourthView: UIView = {
        let view = UIView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .yellow
        
        return view
    }()
    
    let label: UILabel = {
        let label = UILabel()
        
        label.text = "WHAT"
        label.backgroundColor = .white
        
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(firstView)
        view.addSubview(label)
        view.addSubview(secondView)
        view.addSubview(thirdView)
        
        firstView.build()?.safeTop(constant: 10).safeLeft().width(20).height(20).apply()
//        label.build()?.leading(constant: 10).trailing(constant: -10).safeTop(constant: 20).height(400).apply()
        firstView.build()?.center().height(100).width(10).apply()
        secondView.build()?.top(firstView, attribute: .bottom).left().height(100).width(10).apply()
        thirdView.build()?.bottom(firstView, attribute: .top).right().height(100).width(10).apply()
        
    }


}


