//
//  UITextField+Icon.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import UIKit

// TODO: left or right icon
public extension UITextField {
    
    func icon(_ image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 5, width: 20, height: 20))
        let iconContainerView: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 40, height: 30))
        
        iconView.image = image
        iconContainerView.addSubview(iconView)
        
        leftView = iconContainerView
        leftViewMode = .always
        
    }
    
}
