//
//  UIView+UIBuilder.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import UIKit

public extension UIView {
    
    /// Builds a uibuilder with the superview, starts the process for building UI constraints
    func build() -> UIBuilder? {
        guard let constrainedView = self.superview else {
            return nil
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        return UIBuilder(anchoredView: self, constrainedView: constrainedView)
    }
    
    /// Build a constraints builder, starts the process for building UI constraints
    func build(_ constrainedView: UIView) -> UIBuilder {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        return UIBuilder(anchoredView: self, constrainedView: constrainedView)
    }
    
}
