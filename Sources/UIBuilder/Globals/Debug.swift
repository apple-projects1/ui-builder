//
//  Debug.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import Foundation

func debugInfo(file: String = #file, function: String = #function, line: Int = #line) -> String {
    return "FILE: \(file)\n FUNCTION: \(function)\n LINE: \(line)"
}
